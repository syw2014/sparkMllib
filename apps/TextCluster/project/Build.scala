import sbt._
import Keys._
import sbtassembly.Plugin._
import AssemblyKeys._

object TextClusterBuild extends Build {
  lazy val buildSettings = Defaults.defaultSettings ++ Seq(
    version := "0.1",
    organization := "com.shunwang",
    scalaVersion := "2.11.8"
  )
  lazy val app = Project(
    "TextCluster",
    file("."),
    settings = buildSettings ++ assemblySettings ++ Seq(
//      javacOptions ++= Seq("-source", "1.7", "-target", "1.7", "-Xlint"),
      parallelExecution in Test := false,
//      resolvers ++= Seq(Resolver.file("Local Maven Repo", file("E:/repo/repository"))),
      resolvers += "aliyun" at "http://maven.aliyun.com/nexus/content/groups/public/",
      resolvers += "sonatype-oss" at "http://oss.sonatype.org/content/repositories/snapshots",
      resolvers += "spring-oss" at "http://repo.spring.io/plugins-release",
      resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
      resolvers += "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven",
      javacOptions ++= Seq("-encoding", "UTF-8"),
      libraryDependencies ++= Seq(

        "org.apache.spark" % "spark-core_2.11" % "2.0.1",
        "org.apache.spark" % "spark-mllib_2.11" % "2.0.1",
//        "org.apache.spark" % "spark-core_2.11" % "2.0.1" % "provided",
//        "org.apache.spark" % "spark-mllib_2.11" % "2.0.1" % "provided",

        //spark 自身的发布包中不带该组件,需要添加上
//        "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.6.1",
//        "dibbhatt" % "kafka-spark-consumer" % "1.0.6",
//        "org.apache.commons" % "commons-lang3" % "3.4",
        "commons-cli" % "commons-cli" % "1.3.1",
        "org.scalatest" % "scalatest_2.11" % "3.0.0"
      ),
      test in assembly := {},
      //      excludedJars in assembly <<= (fullClasspath in assembly) map { cp =>
      //        cp filter { e =>
      //          println(e.data.getName)
      //          e.data.getName == "jackson-core-2.3.1.jar"
      //        }
      //      },
      mergeStrategy in assembly := {
        case PathList("org", "joda", "base", "BaseDateTime.class", xs@_*) => MergeStrategy.first
        //        case PathList("com", "fasterxml", "jackson",xs@_*) => MergeStrategy.last
        case m if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
        case m if m.toLowerCase.matches("meta-inf.*\\.sf$") => MergeStrategy.discard
        case "log4j.properties" => MergeStrategy.discard
        case m if m.toLowerCase.startsWith("meta-inf\\services\\") => {
          if (m.contains("jackson"))
            MergeStrategy.filterDistinctLines
          else

            MergeStrategy.filterDistinctLines
        }
        case "reference.conf" => MergeStrategy.concat
        case m => MergeStrategy.first
      }
    )
  )
}
