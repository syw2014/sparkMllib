package com.shunwang.clusterTrain


/**
  * Created by yw.shi on 2016/11/03.
  *
  * Text cluster module is designed to combine the samples which are in the same cluster with one sample, then
  * we can use labelled samples to train our classification model based on naive bayes algorithms.
  */

import java.io.File

import com.shunwang.util.{CmdArgument, CmdArguments}
import org.apache.commons.io.FileUtils
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer}
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}

import scala.collection.mutable.HashMap


// according to training data, the first field is sampleId, second field is text content
case class RawDataRecords(sampleId: String, text: String)

object ClusterTrainApp {


  /**
    * Vectorization, convert input samples 'sampleId\t tokenString\t weight ...' into training sample format '(sampleId,array of features)'
    *
    * @param input            input sample with label, 'label,tokenString(separated by space)\tweight ...'
    * @param featureDimension dimension of feature,according to text classification,it can be very large like 10,000
    * @param sampleSize       how many samples will be chosen for every label, to sample balanced,we can adjust the
    *                         accuracy based on different value
    * @return training data, '(sampleId, array of features)'
    */
  def Vectorization(input: RDD[String],
                    featureDimension: Int,
                    sampleSize: Int): DataFrame = {
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    // convert to dataframe
    val dataDf = input.map {
      line => {
        var arr = line.split("\t")
        val (label, token, weight) = (arr(0), arr(1), arr(2).toDouble)
        (label, List((token, weight)))
      }
    }.reduceByKey(_ ::: _)
      .map { case (label, tokens) => (label, tokens.sortWith(_._2 > _._2).take(sampleSize)) }
      .flatMap { case (label, tokenString) => tokenString.map(t => (label, t._1)) }
      .map { case (label, tokenString) => RawDataRecords(label, tokenString) }
      .toDF()

    // segment sample text
    val tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words")
    val sampleTokens = tokenizer.transform(dataDf)

    // word hashing and calculate term frequency
    val hashingTF = new HashingTF().setInputCol("words").setOutputCol("rawfeatures").setNumFeatures(featureDimension)
    val sampleFeature = hashingTF.transform(sampleTokens)

    // calculate term frequency
    val idf = new IDF().setInputCol("rawfeatures").setOutputCol("features")
    val idfModel = idf.fit(sampleFeature)
    idfModel.transform(sampleFeature)
  }

  /**
    * convert training sample with dataframe to rdd labeled point
    *
    * @param input  traing sample,
    * @param field1 select field, now field1 is "sampleId"
    * @param field2 select field, now it's "features"
    * @return training data with rdd labeled point type
    */
  def convert2RddLabeled(input: DataFrame,
                         field1: String,
                         field2: String): RDD[LabeledPoint] = {
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
    input.select(field1, field2).map {
      case Row(label, feature) => {
        val (size, indices, values) = (feature.asInstanceOf[SparseVector].size,
          feature.asInstanceOf[SparseVector].indices,
          feature.asInstanceOf[SparseVector].values)
        LabeledPoint(label.asInstanceOf[String].toDouble, Vectors.sparse(size, indices, values))
      }
    }.rdd
  }

  def main(args: Array[String]): Unit = {


    val argsMap = checkArgs(args)
    val (trainingFile, outputPath, sampleSize, featureDimension, numClusters, numIterations) = (
      argsMap.get("-trainingFile").get,
      argsMap.get("-outputPath").get,
      argsMap.get("-sampleSize").get.toInt,
      argsMap.get("-featureDimension").get.toInt,
      argsMap.get("-numClusters").get.toInt,
      argsMap.get("-numIterations").get.toInt
      )

    val spark = SparkSession
      .builder()
      .appName("ClusterTrainApp")
      //      .master("local[2]")
      .config("spark.sql.warehouse.dir", "file:///")
      .getOrCreate()

    //    val trainingFile = "data/samples_5k_id.txt"
    //    val outputPath = "result/predict/"
    //    val featureDimension = 1000
    //    val sampleSize = 10000
    //    val numClusters = 100
    //    val numIterations = 5000

    val trainRawData = spark.sparkContext.textFile(trainingFile)
    val sampleFeatures = Vectorization(trainRawData, featureDimension, sampleSize)
    //    sampleFeatures.select("sampleId","rawfeatures").take(5).foreach(println)

    val featuresInfo = convert2RddLabeled(sampleFeatures, "sampleId", "features")

    val trainingFeatures = sampleFeatures.select("features").rdd.map {
      case Row(rawfeatures) =>
        val s = rawfeatures.asInstanceOf[SparseVector]
        Vectors.sparse(s.size, s.indices, s.values)
    }.cache()



    val clusters = KMeans.train(trainingFeatures, numClusters, numIterations)

    // test cluster result
    val predictRes = featuresInfo.map(
      v => (v.label, clusters.predict(v.features))
    )
    //    predictRes.take(10).foreach(println)
    predictRes.saveAsTextFile(outputPath)

    println("WSSSE : " + clusters.computeCost(trainingFeatures))
    //    clusters.clusterCenters.foreach(println)

    spark.stop()
  }

  // cmd parameter parse
  /**
    *
    * @param args
    * @return
    */
  def checkArgs(args: Array[String]): HashMap[String, String] = {
    //参数相关的
    val cmdArgs = Array[CmdArgument](
      CmdArgument("-trainingFile", "训练样本，格式为（sampleId\ttokenString\tweight）"),
      CmdArgument("-outputPath", "最终每个样本预测结果存储路径，结果格式为（sampleId,）"),
      CmdArgument("-sampleSize", "样本筛选，根据每个sample的weight，即每个sample属于某个label的概率大小进行筛选"),
      CmdArgument("-featureDimension", "每个sample的特征维数"),
      CmdArgument("-numClusters", "聚类的簇数即将样本聚成多少个类，调节原则是样本大小的10%，默认：1000"),
      CmdArgument("-numIterations", "模型迭代次数，调节原则是随样本数成反比否则需要消耗很多的资源，默认：2000")
    )
    val cmdArgments = new CmdArguments(cmdArgs)
    var cmdParsedMap: HashMap[String, String] = new HashMap[String, String]()
    try {
      cmdParsedMap = cmdArgments.checkArgs(args)
    } catch {
      case e =>
        println(e.getMessage)
        cmdArgments.printHelp()
        System.exit(-1)
    }
    cmdParsedMap
  }


}

// object end
