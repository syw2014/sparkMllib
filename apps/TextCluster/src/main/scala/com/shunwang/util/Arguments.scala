package com.shunwang.util

import scala.collection.mutable.HashMap

/**
  * Created by tj.wan on 2015-10-29.
  * 该类提供给所有的类统一的参数检测接口,方便编写
  * 抽象出的概念就是CmdArgument,特性: name-名称,isNull-是否可空,defaultValue-默认值
  */
case class CmdArgument(name: String, desc: String = "", var value: String = "", isNull: Boolean = false, defaultValue: String = "")


class CmdArguments(cmdArguments: Array[CmdArgument]) {
  private val argMap = new HashMap[String, CmdArgument]()

  //保证cmdArgment中的数据和argMap中的数据一致
  for (cmdArgment <- cmdArguments) {
    argMap.put(cmdArgment.name, cmdArgment)
  }

  /**
    * 参数解析
    *
    * @return
    */
  def checkArgs(args: Array[String]): HashMap[String, String] = {
    //参数名称检测,参数名称必须以"-"开头的连续字符串,参数值不允许以"-"开头
    if (args.size == 0)
      throw new IllegalArgumentException("argments can not empty")
    var i = 0
    while (i < args.length) {
      if (args(i).startsWith("-")) {
        if (args(i + 1).startsWith("-")) {
          //某命令参数下一个字符串也为命令参数时
          argMap.get(args(i)) match {
            case Some(c) =>
              if (c.isNull)
                c.value = c.defaultValue
              else
                throw new IllegalArgumentException(s"argment ${args(i)} is empty,please reset")
            case None =>
              throw new IllegalArgumentException(s"argment ${args(i)} is invalid argment,please remove")
          }
          //          if(argMap.get(args(i))  argMap.put(args(i), "")
          println(s"${args(i)} value is empty,plase reset")
          System.exit(0)
        } else {
          argMap.get(args(i)) match {
            case Some(c) => c.value = args(i + 1)
            case None =>
              throw new IllegalArgumentException(s"argment ${args(i)} is invalid argment,please remove")
          }
          i += 2
        }
      } else {
        //在一个命令参数后面跟随多个字符串的情况下,只有第一个字符串会作为参数的值,只是需要注意的
        i += 1
      }
    }
    argMap.map(kv => (kv._1, kv._2.value))
  }

  def printHelp(): Unit = {
    println("Usage:\t")
    for (cmdArg <- argMap) {
      println(s"\targment ${cmdArg._1}\tdesc ${cmdArg._2.desc}")
    }
    //    println("参数问题,系统退出")
    ////    System.exit(-1)
  }


}
