package com.shunwang.util


import breeze.linalg.{DenseVector => BDV}
import org.apache.spark.mllib.linalg.{DenseVector, SparseVector, Vector, Vectors}
import org.apache.spark.rdd.RDD

/**
  * Created by yw.shi on 2016/11/17.
  */

class UIDF(val minDocFreq: Int) {

  def this() = this(0)

  def fit(dataset: RDD[Vector]): UIDFModel = {
    val idf = dataset.treeAggregate(new UIDF.DocumentFrequencyAggregator(minDocFreq)
    )(seqOp = (df, v) => df.add(v),
      combOp = (df1, df2) => df1.merge(df2)).idf()
    new UIDFModel(idf)
  }
}

// end class UIDF

private object UIDF {

  /** document frequency aggregator, realized based on IDF method in mllib idf */
  class DocumentFrequencyAggregator(val minDocFreq: Int) extends Serializable {

    /** numbers of document, according to this, which means the number of labels */
    private var m = 0L

    /** document frequency vector */
    private var df: BDV[Long] = _

    def this() = this(0)

    /** Add a new document. **/
    def add(doc: Vector): this.type = {
      if (isEmpty) {
        df = BDV.zeros(doc.size) // initial
      }
      doc match {
        case SparseVector(size, indices, values) =>
          val nnz = indices.length
          var k = 0
          while (k < nnz) {
            if (values(k) > 0) {
              df(indices(k)) += 1L
            }
            k += 1
          }
        case DenseVector(values) =>
          val n = values.length
          var j = 0
          while (j < n) {
            if (values(j) > 0.0) {
              df(j) += 1L
            }
            j += 1
          }
        case other =>
          throw new UnsupportedOperationException(
            s"only sparse and dense vector are supported but got ${other.getClass}. "
          )
      }
      m += 1L
      this
    }

    /** merge another. */
    def merge(ohter: DocumentFrequencyAggregator): this.type = {
      if (!ohter.isEmpty) {
        m += ohter.m
      }
      if (df == null) {
        df = ohter.df.copy
      } else {
        df += ohter.df
      }
      this
    }

    private def isEmpty: Boolean = m == 0L

    /** Return the current IDF vector. */
    def idf(): Vector = {
      if (isEmpty) {
        throw new IllegalStateException("Haven't seen any document yet.")
      }
      val n = df.length
      val inv = new Array[Double](n)
      var j = 0
      while (j < n) {
        /** If the term is not present in the minimum
          * number of documents, set IDF to 0. This will cause multiplication in IDFModel to
          * set TF-IDF to 0.
          * Since arrays are initialized to 0 by default,
          * we just omit changing those entries.
          * */
        if (df(j) >= minDocFreq) {
          inv(j) = math.log((m + 1.0) / (df(j) + 1.0))
        }
        j += 1
      }
      Vectors.dense(inv)
    }
  }

}

// end object UIDF

/**
  * Represents an IDF model that can transform term frequency vector
  */
class UIDFModel(val idf: Vector) extends Serializable {

  /**
    * Transforms term frequency (TF) vectors to TF-IDF vectors.
    *
    * If `minDocFreq` was set for the IDF calculation,
    * the terms which occur in fewer than `minDocFreq`
    * documents will have an entry of 0.
    *
    * @param dataset an RDD of term frequency vectors
    * @return an RDD of TF-IDF vectors
    */
  def transform(dataset: RDD[Vector]): RDD[Vector] = {
    val bcIdf = dataset.context.broadcast(idf)
    dataset.mapPartitions(iter => iter.map(v => UIDFModel.transform(bcIdf.value, v)))
  }

  /** Transform a term frequency vector to TF-IDF vector.
    *
    * @param v a term frequency vector
    * @return a TF-IDF vector
    **/
  def transform(v: Vector): Vector = UIDFModel.transform(idf, v)

}

// end class UIDFModel

private object UIDFModel {

  /**
    * Transform a term frequency vector to a tf-idf vector with idf vector,remember the
    * order of idf vector and tf vector is the same, which means that the index of the same term is
    *
    * @param idf an IDF vector
    * @param v   term frequency vector
    * @return a tf-idf vector
    **/
  def transform(idf: Vector, v: Vector): Vector = {
    val n = v.size
    v match {
      case SparseVector(size, indices, values) =>
        val nnz = indices.length
        val newValues = new Array[Double](nnz)
        var k = 0
        while (k < nnz) {
          newValues(k) = values(k) * idf(indices(k))
          k += 1
        }
        Vectors.sparse(size, indices, newValues)

      case DenseVector(values) =>
        val newValues = new Array[Double](n)
        var j = 0
        while (j < n) {
          newValues(j) = values(j) * idf(j)
          j += 1
        }
        Vectors.dense(newValues)
      case other =>
        throw new UnsupportedOperationException(
          s"Only sparse and dense vectors are supported but got ${other.getClass}.")
    }
  }

}

// end UIDFModule object
