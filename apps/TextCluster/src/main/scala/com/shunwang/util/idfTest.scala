package com.shunwang.util

import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer}
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.{Row, SparkSession}

case class RawDataRecords(label: String, rawFeatures: SparseVector)

/**
  * Created by yw.shi on 2016/11/16.
  */
object idfTest {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .appName("ClusterTrainApp")
      .master("local[2]")
      .config("spark.sql.warehouse.dir", "file:///")
      .getOrCreate()

    val sentenceData = spark.createDataFrame(Seq(
      (0, "Hi I heard about Spark case case"),
      (0, "I wish Java could use case classes classes"),
      (1, "Logistic regression models are neat I use")
    )).toDF("label", "sentence")

    val tokenizer = new Tokenizer().setInputCol("sentence").setOutputCol("words")
    val wordsData = tokenizer.transform(sentenceData)
    val hashingTF = new HashingTF()
      .setInputCol("words").setOutputCol("rawFeatures").setNumFeatures(50)
    val featurizedData = hashingTF.transform(wordsData)
    // alternatively, CountVectorizer can also be used to get term frequency vectors

    featurizedData.take(5).foreach(println)

    import spark.implicits._
    val scalaTF = featurizedData.select("label", "rawFeatures").rdd
      .map {
        case Row(label, tfv) =>
          (label, List(tfv.asInstanceOf[SparseVector]))
      }
      .reduceByKey(_ ::: _)
      .map { case (label, tfv) => {
        val ss = tfv.head
        val newValues = new Array[Double](ss.size)
        for (s <- tfv) {
          var k = 0
          while (k < s.size) {
            newValues(k) += s.toDense.toArray.apply(k);
            k += 1
          }
        }
        (label, Vectors.dense(newValues).toSparse)
      }
      }.map { case (label, nfeatures) => RawDataRecords(label.toString, nfeatures) }
      .toDF()
    scalaTF.select("label", "rawFeatures").take(5).foreach(println)

    val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
    val idfModel = idf.fit(scalaTF)

    val rescaledData = idfModel.transform(featurizedData)
    rescaledData.select("features", "label").take(3).foreach(println)
  }


}
