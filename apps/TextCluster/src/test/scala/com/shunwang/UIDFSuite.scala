
package com.shunwang.util

import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.FunSuite
import com.shunwang.util.UIDF
import com.shunwang.util.TestingUtils._

import org.apache.spark.mllib.linalg.{DenseVector, SparseVector, Vector, Vectors}



/**
 * Created by yw.shi on 2016-11-21.
 */
class UIDFSuite extends FunSuite{

  val spark = SparkSession
    .builder()
    .master("local[2]")
    .config("spark.sql.warehouse.dir", "file:///")
    .getOrCreate()

    test("idf") {
      val n = 4
      val localTermFrequencies = Seq(
        Vectors.sparse(n, Array(1, 3), Array(1.0, 2.0)),
        Vectors.dense(0.0, 1.0, 2.0, 3.0),
        Vectors.sparse(n, Array(1), Array(1.0))
      )
      val m = localTermFrequencies.size
      val termFrequencies = spark.sparkContext.parallelize(localTermFrequencies, 2)
      val idf = new UIDF
      val model = idf.fit(termFrequencies)
      val expected = Vectors.dense(Array(0, 3, 1, 2).map { x =>
        math.log((m + 1.0) / (x + 1.0))
      })
      assert(model.idf ~== expected absTol 1e-12)

      val assertHelper = (tfidf: Array[Vector]) => {
        assert(tfidf.size === 3)
        val tfidf0 = tfidf(0).asInstanceOf[SparseVector]
        assert(tfidf0.indices === Array(1, 3))
        assert(Vectors.dense(tfidf0.values) ~==
          Vectors.dense(1.0 * expected(1), 2.0 * expected(3)) absTol 1e-12)
        val tfidf1 = tfidf(1).asInstanceOf[DenseVector]
        assert(Vectors.dense(tfidf1.values) ~==
          Vectors.dense(0.0, 1.0 * expected(1), 2.0 * expected(2), 3.0 * expected(3)) absTol 1e-12)
        val tfidf2 = tfidf(2).asInstanceOf[SparseVector]
        assert(tfidf2.indices === Array(1))
        assert(tfidf2.values(0) ~== (1.0 * expected(1)) absTol 1e-12)
      }
      // Transforms a RDD
      val tfidf = model.transform(termFrequencies).collect()
      assertHelper(tfidf)
      // Transforms local vectors
      val localTfidf = localTermFrequencies.map(model.transform(_)).toArray
      assertHelper(localTfidf)
    }

    test("idf minimum document frequency filtering") {
      val n = 4
      val localTermFrequencies = Seq(
        Vectors.sparse(n, Array(1, 3), Array(1.0, 2.0)),
        Vectors.dense(0.0, 1.0, 2.0, 3.0),
        Vectors.sparse(n, Array(1), Array(1.0))
      )
      val m = localTermFrequencies.size
      val termFrequencies = spark.sparkContext.parallelize(localTermFrequencies, 2)
      val idf = new UIDF(minDocFreq = 1)
      val model = idf.fit(termFrequencies)
      val expected = Vectors.dense(Array(0, 3, 1, 2).map { x =>
        if (x > 0) {
          math.log((m + 1.0) / (x + 1.0))
        } else {
          0
        }
      })
      assert(model.idf ~== expected absTol 1e-12)

      val assertHelper = (tfidf: Array[Vector]) => {
        assert(tfidf.size === 3)
        val tfidf0 = tfidf(0).asInstanceOf[SparseVector]
        assert(tfidf0.indices === Array(1, 3))
        assert(Vectors.dense(tfidf0.values) ~==
          Vectors.dense(1.0 * expected(1), 2.0 * expected(3)) absTol 1e-12)
        val tfidf1 = tfidf(1).asInstanceOf[DenseVector]
        assert(Vectors.dense(tfidf1.values) ~==
          Vectors.dense(0.0, 1.0 * expected(1), 2.0 * expected(2), 3.0 * expected(3)) absTol 1e-12)
        val tfidf2 = tfidf(2).asInstanceOf[SparseVector]
        assert(tfidf2.indices === Array(1))
        assert(tfidf2.values(0) ~== (1.0 * expected(1)) absTol 1e-12)
      }
      // Transforms a RDD
      val tfidf = model.transform(termFrequencies).collect()
      assertHelper(tfidf)
      // Transforms local vectors
      val localTfidf = localTermFrequencies.map(model.transform(_)).toArray
      assertHelper(localTfidf)
    }

}
