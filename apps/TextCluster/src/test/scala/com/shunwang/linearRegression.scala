package com.shunwang
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.sql.SparkSession

/**
  * Created by yw.shi on 2016/11/16.
  */
object linearRegression {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .appName("ClusterTrainApp")
      .master("local[2]")
      .config("spark.sql.warehouse.dir", "file:///")
      .getOrCreate()

    // Load training data
    val training = spark.read.format("libsvm")
      .load("D:/spark-2.0.0-bin-hadoop2.6/data/mllib/sample_linear_regression_data.txt")

    val lr = new LinearRegression()
      .setMaxIter(10)
      .setRegParam(0.3)
      .setElasticNetParam(0.8)

    // Fit the model
    val lrModel = lr.fit(training)

    // Print the coefficients and intercept for linear regression
    println(s"Coefficients: ${lrModel.coefficients} Intercept: ${lrModel.intercept}")

    // Summarize the model over the training set and print out some metrics
    val trainingSummary = lrModel.summary
    println(s"numIterations: ${trainingSummary.totalIterations}")
    println(s"objectiveHistory: ${trainingSummary.objectiveHistory.toList}")
    trainingSummary.residuals.show()
    println(s"RMSE: ${trainingSummary.rootMeanSquaredError}")
    println(s"r2: ${trainingSummary.r2}")
  }

}
