
import java.util.UUID

import org.apache.commons.cli.{DefaultParser, Options}
import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.FunSuite
import com.shunwang.clusterTrain.ClusterTrainApp


/**
 * Created by tj.wan on 2016-03-18.
 */
class ClusterSuite extends FunSuite{

  val spark = SparkSession
    .builder()
    .appName("ClusterTrainApp")
    .master("local[2]")
    .config("spark.sql.warehouse.dir", "file:///")
    .getOrCreate()

  test("Text Cluster Train"){

        val trainingFile = "data/samples_5k_id.txt"
        val outputPath = "result/predict/"
        val featureDimension = 1000
        val sampleSize = 10000
        val numClusters = 10
        val numIterations = 300

    val trainRawData = spark.sparkContext.textFile(trainingFile)
    val sampleFeatures =
      ClusterTrainApp.Vectorization(trainRawData, featureDimension, sampleSize)
        sampleFeatures.select("sampleId","rawfeatures").take(5).foreach(println)

    val featuresInfo = ClusterTrainApp.convert2RddLabeled(sampleFeatures, "sampleId", "features")

    val trainingFeatures = sampleFeatures.select("features").rdd.map{
      case Row(rawfeatures) =>
        val s = rawfeatures.asInstanceOf[SparseVector]
        Vectors.sparse(s.size,s.indices,s.values)
    }.cache()

    val clusters = KMeans.train(trainingFeatures, numClusters, numIterations)

    // test cluster result
    val predictRes = featuresInfo.map(
      v => (v.label, clusters.predict(v.features))
    )
//    predictRes.take(10).foreach(println)
    predictRes.saveAsTextFile(outputPath)

    println("WSSSE : " + clusters.computeCost(trainingFeatures))
    //    clusters.clusterCenters.foreach(println)
  }

}
